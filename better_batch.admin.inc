<?php

/**
 * @file
 * Holds the administration page and its helper functions.
 */

/**
 * Form callback for configuration page.
 */
function better_batch_settings_form() {
  $form = array();

  // Fieldset for general improvement settings.
  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General improvements'),
  );
  // Global Batch jobs.
  $form['general']['better_batch_global_jobs'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('better_batch_global_jobs', FALSE),
    '#title' => t('Global batch jobs'),
    '#description' => t('Progress pages of global batches are reachable by multiple users. Access validation depends on the users permission to visit the batch source page. Only the batch owner pushes the process, other users only get the current batch state.'),
  );
  // Session independence.
  $form['general']['better_batch_session_independence'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('better_batch_session_independence', FALSE),
    '#title' => t('Session independence'),
    '#description' => t('The dependence of batch jobs on sessions will be released and bound to the user instead. So batch jobs are still available after re-login.'),
  );
  // Unique non-concurrent Batch jobs.
  $form['general']['better_batch_unique_jobs'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('better_batch_unique_jobs', FALSE),
    '#title' => t('Unique non-concurrent batch jobs'),
    '#description' => t('Equal batch jobs will not be started twice if there is one already running.'),
  );
  // Clean batch URLs.
  $form['general']['better_batch_clean_urls'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('better_batch_clean_urls', FALSE),
    '#title' => t('Clean batch URLs'),
    '#description' => t("Use clean URLs for batch paths: '/batch/start/6' will be used instead of 'batch?op=start&id=6'."),
  );
  // Batch source URL.
  $form['general']['better_batch_use_source'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('better_batch_use_source', FALSE),
    '#title' => t('Use source URL'),
    '#description' => t("Use batch source URL instead of default batch path. If a batch was started from admin/reports/updates/check this path will be used instead of 'batch?op=start&id=6'."),
  );

  // Progress Bar style.
  $form['style'] = array(
    '#type' => 'fieldset',
    '#title' => t('Progress bar style'),
  );
  // Inner progress bar style.
  $form['style']['better_batch_inner_style'] = array(
    '#type' => 'select',
    '#title' => t('Inner progress bar style'),
    '#options' => _better_batch_inner_styles(),
    '#default_value' => variable_get('better_batch_inner_style', 'default'),
    '#description' => t('Change the style of the inner progress bar element.'),
  );
  $form['style']['better_batch_inner_style_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Inner progress bar color'),
    '#default_value' => variable_get('better_batch_inner_style_color', '#0072B9'),
    '#size' => 10,
    '#maxlength' => 7,
    '#description' => t('Change the style of the inner progress bar element.'),
    '#states' => array(
      'visible' => array(
        ':input[name="better_batch_inner_style"]' => array('value' => 'color'),
      ),
    ),
  );
  $form['style']['better_batch_inner_style_image'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom image'),
    '#default_value' => variable_get('better_batch_inner_style_image', ''),
    '#description' => t('Image of the inner progress bar element.'),
    '#states' => array(
      'visible' => array(
        ':input[name="better_batch_inner_style"]' => array('value' => 'image'),
      ),
    ),
  );
  // Outer progress bar color.
  $form['style']['better_batch_outer_style'] = array(
    '#type' => 'textfield',
    '#title' => t('Outer progress bar color'),
    '#default_value' => variable_get('better_batch_outer_style', '#CCCCCC'),
    '#size' => 10,
    '#maxlength' => 7,
    '#description' => t('Change the style of the outer progress bar element. Leave empty for default.'),
  );
  return system_settings_form($form);
}

/**
 * Returns an array of inner progress bar styles.
 */
function _better_batch_inner_styles() {
  $images_path = base_path() . drupal_get_path('module', 'better_batch') . '/images/';
  $styles = array(
    'default' => t('Default'),
    $images_path . 'progress-blue-backwards.gif' => t('Default, Backwards'),
    $images_path . 'progress-grayed.gif' => t('Default, Grayed'),
    $images_path . 'progress-light-grayed.gif' => t('Default, (light) Grayed'),
    $images_path . 'progress-inverted.gif' => t('Default, Inverted'),
    $images_path . 'progress-red.gif' => t('Default, Red'),
    $images_path . 'progress-green.gif' => t('Default, Green'),
    $images_path . 'progress-yellow.gif' => t('Default, Yellow'),
    $images_path . 'progress-purple.gif' => t('Default, Purple'),
    'color' => t('Custom color'),
    'image' => t('Custom image'),
  );
  return $styles;
}
