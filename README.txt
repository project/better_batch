-- SUMMARY --

Better Batch

This module improves and extends functionality of the Batch API.


-- REQUIREMENTS --

none.


-- FEATURES --

Global batch jobs (Experimental!)
  Progress pages of global batches are reachable by multiple users.
  Access validation depends on the users permission to visit the batch
  source page. Only the batch owner pushes the process, other users only
  get the current batch state.

Session independence
  The dependence of batch jobs on sessions will be released and bound to
  the user instead. So batch jobs are still available after re-login.

Unique non-concurrent batch jobs
  Equal batch jobs will not be started twice if there is one already running.

Use source URL (Experimental!)
  Use batch source URL instead of default batch path. If a batch was started
  from admin/reports/updates/check this path will be used instead of
  'batch?op=start&id=6'.

Modify default progress bar style
  Change the color of the default progress bar animation (eg. green, red, gray,
  ..), use custom colors or a custom image.


-- USAGE --

Install as usual, see http://drupal.org/node/895232 for further information.

Configuration
  After installation go to admin/config/system/batch/better and select the
  improvements you want to use on your site.
  Note that some of them are still experimental, use them with care!


-- NOTE --

This module can be used with Drupal's core Batch API but was optimized for the
Background Batch module (a submodule of Background Process
[http://drupal.org/project/background_process]). To work properly you have to
be sure that Better Batch was installed after Background Batch!
